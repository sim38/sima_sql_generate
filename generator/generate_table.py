"""
Класс GenerateTable для генерации таблицы
"""
from mysql.connector import connect
from mysql.connector.cursor_cext import CMySQLCursor
from generator.SQL import SQLGen
from generator.model_data import Model


class GenerateTable(SQLGen):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.Host = kwargs.get('host')
        self.Port = kwargs.get('port')
        self.User = kwargs.get('user')
        self.Password = kwargs.get('password')
        self.Db = kwargs.get('db')
        self.model: Model = kwargs.get('model')

    def run(self, count: int = 1000):
        with connect(host=self.Host, port=self.Port,
                     user=self.User, password=self.Password,
                     db=self.Db, ) as connection:
            with connection.cursor(buffered=True) as cur:
                self.insert_request(cur, count)
                connection.commit()

    def insert_request(self, cur: CMySQLCursor, count: int):
        cur.execute(self._get_insert(count=count))
