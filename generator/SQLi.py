
import random

from generator.model_data import Model, Field
from generator.SQLABC import Template, SQLiTemplate
from generator.SQL import SQLGen


class SQLiGen(SQLGen):
    def __init__(self, model: Model, *args, **kwargs):
        super().__init__(model)
        self.injection = self.__get_injection()

    def _get_where(self, fields: list[Field] = None) -> str:
        if fields is None:
            fields = self.model.random_count_field
        res = Template.WHERE_BASE_REQUEST
        for field in fields:
            if res != Template.WHERE_BASE_REQUEST:
                res += ' and '
            if not self.requests.last_request.Injection:
                val = random.choice(self.injection)
                self.requests.last_request.Injection = True
            else:
                val = field.values
            res += Template.WHERE_REQUEST.format(
                field.Name,
                random.choice(field.Base_data_type.WhereOperators),
                val
            )
        return res

    def __get_injection(self):
        res = []
        for i in range(1, len(self.model.Fields) + 1):
            res.append(SQLiTemplate.IntOrderByInjection.format(i))
        return res

    def get_select_request(self) -> str:
        new_request = self.requests.new_request()
        if new_request.SelectTemplateGroupBy:
            new_request.GroupBy, self.requests.last_request.GroupByField = self._get_group_by()
        if new_request.SelectTemplateJoin:
            new_request.Join = self._get_join()
        new_request.Where = self._get_where()
        if new_request.SelectTemplateHaving:
            new_request.Having = self._get_having()
        if new_request.SelectTemplateOrderBy:
            new_request.OrderBy = self._get_order_by()
        new_request.Select = self._get_select()
        return new_request.request
