import random

from abc import ABC, abstractmethod


class SQL(ABC):
    @abstractmethod
    def get_select_request(self):
        pass

    @abstractmethod
    def get_insert_request(self):
        pass

    @abstractmethod
    def get_update_request(self):
        pass


class Template:
    # https://www.w3schools.com/mysql/mysql_select.asp
    SELECT_REQUEST = 'SELECT {} FROM {} '  # DISTINCT, COUNT
    INSERT_REQUEST = 'INSERT INTO {} ({}) VALUES {} '
    DELETE_REQUEST = 'DELETE FROM {} '
    UNION_REQUEST = '{} {} UNION {} {}'
    WHERE_BASE_REQUEST = 'WHERE '
    WHERE_REQUEST = '{} {} {} '  # =, >, <, >=, <=, <>, BETWEEN, LIKE, IN, NOT, IS NULL, IS NOT NULL
    GROUP_BY_REQUEST = 'GROUP BY {} '
    ORDER_BY_REQUEST = 'ORDER BY {} {} '  # ASC, DESC
    HAVING_REQUEST = 'HAVING {} {} {} '  # SUM, COUNT, MIN, MAX
    UPDATE_REQUEST = 'UPDATE {} SET {} '
    JOIN_REQUEST = '{} JOIN {} ON {} = {} '

    ORDER_BY_SORT = ['ASC', 'DESC']
    HAVING_AGG_FUNC = ['SUM({})', 'COUNT({})', 'MIN({})', 'MAX({})']
    HAVING_OPERATORS = ['>', '<', '=', '<>', '>=', '<=']
    JOIN_TYPE = ['INNER', 'LEFT', 'RIGHT', 'CROSS']


class SQLiTemplate:
    IntOrderByInjection = "'' ORDER BY {} ;--"  # вставлять int значения
    NullUnionInjection = "'' UNION SELECT {} ;--"  # необходимое количество NULL через запятую
    OnlyTrue = "'' OR 1=1 ;--"
    DropTable = "''; DROP TABLE {}; --"  # имя таблицы


class Request:
    Select = ''
    Join = ''
    Where = ''
    GroupBy = ''
    Having = ''
    OrderBy = ''

    GroupByField = ''

    def __init__(self):
        self.SelectTemplateJoin = random.choice([0, 1])
        self.SelectTemplateWhere = random.choice([0, 1])
        self.SelectTemplateGroupBy = random.choice([0, 1])
        self.SelectTemplateHaving = random.choice([0, 1]) * self.SelectTemplateGroupBy
        self.SelectTemplateOrderBy = random.choice([0, 1])
        self.Injection = False

    @property
    def request(self) -> str:
        return self.Select + self.Join + self.Where + self.GroupBy + self.Having + self.OrderBy


class Requests:
    AllRequest = []

    def new_request(self) -> Request:
        new = Request()
        self.AllRequest.append(new)
        return new

    @property
    def last_request(self) -> Request:
        return self.AllRequest[-1]
