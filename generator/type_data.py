import random

from abc import ABC, abstractmethod
from datetime import date, datetime, timedelta


class ABCClassType(ABC):
    Type: type
    Name: list
    Shield: bool
    AggrFunc: list

    WhereOperators: list
    HavingOperators: list

    HavingAggrFunc: list

    @staticmethod
    @abstractmethod
    def convert(value):
        pass

    @property
    @abstractmethod
    def value(self) -> str:
        pass

    def shield(self, value) -> str:
        if self.Shield:
            return "'" + str(value) + "'"
        return str(value)


class StrType(ABCClassType):
    Type = str
    Name = ['varchar', 'char', 'text']
    WhereOperators = ['=']
    Shield = True

    AggrFunc = ['COUNT({})', 'MIN({})', 'MAX({})']

    @staticmethod
    def convert(value) -> str:
        return str(value)

    @property
    def value(self):
        return self.shield(random.choice(list('qwertyuiopasdfghjklzxcvbnm')))


class BoolType(ABCClassType):
    Type = bool
    Name = ['tinyint']
    WhereOperators = ['=', 'IS', 'IS NOT']
    Shield = False

    AggrFunc = ['COUNT({})', 'MIN({})', 'MAX({})']

    @staticmethod
    def convert(value) -> bool:
        return bool(value)

    @property
    def value(self):
        return self.shield(random.choice([True, False]))


class IntType(ABCClassType):
    Type = int
    Name = ['int', 'blob']
    WhereOperators = ['=', '>', '<', '>=', '<=', '<>']
    Shield = False

    AggrFunc = ['COUNT({})', 'MIN({})', 'MAX({})']

    @staticmethod
    def convert(value):
        return int(value)

    @property
    def value(self):
        return self.shield(random.randint(1, 10))


class DateType(ABCClassType):
    Type = date
    Name = ['date']
    WhereOperators = ['<', '>']
    Shield = True

    AggrFunc = ['COUNT({})', 'MIN({})', 'MAX({})']

    @staticmethod
    def convert(value):
        dt = DateTimeType.convert(value)
        return date(year=dt.year, month=dt.month, day=dt.day)

    @property
    def value(self):
        return self.shield(
            date(year=random.randint(1000, 3000), month=random.randint(1, 12), day=random.randint(1, 28))
        )


class DateTimeType(ABCClassType):
    Type = date
    Name = ['datetime']
    WhereOperators = ['<', '>']
    Shield = True

    AggrFunc = ['COUNT({})', 'MIN({})', 'MAX({})']

    @staticmethod
    def convert(value):
        try:
            return datetime.strptime(value, '%Y-%m-%d %H:%M:%S.%f')
        except:
            return datetime.strptime(value, "'%Y-%m-%d %H:%M:%S.%f'")

    @property
    def value(self):
        return self.shield(datetime.now() + timedelta(days=random.randint(1, 10000)) * random.choice([1, -1]))


AllType = [IntType, StrType, BoolType, DateType, DateTimeType]
SQLiType = [
    "' ORDER BY {} ;--",  # вставлять int значения
    "' UNION SELECT {} ;--",  # необходимое количество NULL через запятую
    "' OR 1=1 ;--",
    "'; DROP TABLE {}; --",  # имя таблицы
]
