class FieldNotFound(Exception):
    ...


class ModelNotFound(Exception):
    ...


class TypeFieldNotFound(Exception):
    ...


class TypeConvertError(Exception):
    ...


class FieldKeyIsNone(Exception):
    ...


class ErrorSQLRequest(Exception):
    ...
