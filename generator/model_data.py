import random

from mysql.connector import connect
from mysql.connector.cursor_cext import CMySQLCursor
from typing import Union
from dataclasses import dataclass

from generator.type_data import AllType, ABCClassType
from generator.errors import TypeFieldNotFound, ModelNotFound


@dataclass
class Field:
    Name: str
    DefaultName: str
    Type: type
    Null: bool
    Key: str
    Default: Union[str, None]

    FCValues: list

    Base_data_type: ABCClassType

    def __init__(self, *args, **kwargs):
        self.FCValues = kwargs.get('fc_values')
        self.Name = kwargs.get('name')
        self.DefaultName = self.Name.split('`')[-2]
        self.Base_data_type = None
        for i in AllType:
            for j in i.Name:
                if j in kwargs.get('type'):
                    self.Base_data_type = i()
                    break
        if not self.Base_data_type:
            raise TypeFieldNotFound(f'Тип данных {kwargs.get("type")} нет в системе')
        self.Type = self.Base_data_type.Type
        self.Null = True if kwargs.get('null') == 'YES' else False
        self.Key = kwargs.get('key')
        self.Default = kwargs.get('default')

    @property
    def key_database(self):
        if self.Key:
            return self.Key.split('.')[0]

    @property
    def key_field(self):
        if self.Key:
            return self.Key.split('.')[1]

    @property
    def values(self):
        if self.Key:
            return self.Base_data_type.shield(random.choice(self.FCValues))
        else:
            return self.Base_data_type.value


@dataclass
class Model:
    Name: str
    DefaultName: str
    Primary_key: Field
    Fields: tuple[Field]

    def __init__(self, *args, **kwargs):
        self.Name = '`' + kwargs.get('Name') + '`'
        self.DefaultName = kwargs.get('Name')
        self.Fields = self.__get_fields(kwargs.get('cur'))

    def __get_references(self, cur: CMySQLCursor, field_name: str):
        cur.execute(f'SHOW CREATE TABLE {self.Name}')
        try:
            res = (cur.fetchall())[0][1].split(f'FOREIGN KEY (`{field_name}`)')[1].split('`')
            return f'`{res[1]}`.`{res[3]}`'
        except:
            return None

    def __get_fields(self, cur):
        cur.execute(f"describe {self.Name}")
        fields = []
        for i in cur.fetchall():
            name = f'`{self.DefaultName}`.`{i[0]}`'
            if i[3] == 'MUL':
                fc = self.__get_references(cur, i[0])
                fc_values = self.__get_fc_values(cur, fc)
                fields.append(
                    Field(name=name, type=i[1].decode("utf-8"), null=i[2], key=fc, default=i[4], fc_values=fc_values)
                )
            elif i[3] != 'PRI':
                fields.append(Field(name=name, type=i[1].decode("utf-8"), null=i[2], key=None, default=i[4]))
            else:
                self.Primary_key = Field(name=name, type=i[1].decode("utf-8"), null=i[2], key=i[3], default=i[4])
        return tuple(fields)

    def __get_fc_values(self, cur, fc):
        cur.execute(f"SELECT {fc.split('.')[1]} FROM {fc.split('.')[0]}")
        return [i[0] for i in cur.fetchall()]

    @property
    def random_field(self) -> Field:
        return random.choice(self.Fields)

    @property
    def random_count_field(self) -> list:
        """
        :return: случайное количество из доступных полей таблицы, в кайнем случае вернет 1 случайное значение
        """
        res = [field for field in self.Fields if random.choice([False, True])]
        if not res:
            return [self.random_field]
        return res

    @property
    def random_foreign_key_field(self) -> Union[Field, None]:
        r = [field for field in self.Fields if field.Key]
        if not r:
            return None
        return random.choice(r)


@dataclass
class Database:
    Host: str
    Port: int
    User: str
    Password: str
    Db: str

    Tables: tuple[Model]

    def __init__(self, *args, **kwargs):
        self.Host = kwargs.get('Host')
        self.Port = kwargs.get('Port')
        self.User = kwargs.get('User')
        self.Password = kwargs.get('Password')
        self.Db = kwargs.get('Db')
        self.Models = self.__get_tables()

    @staticmethod
    def __get_all_table(cur) -> list:
        cur.execute("show tables;")
        return [i[0] for i in cur.fetchall()]

    def __get_tables(self) -> tuple[Model]:
        with connect(host=self.Host, port=self.Port,
                     user=self.User, password=self.Password,
                     db=self.Db,) as connection:
            with connection.cursor(buffered=True) as cur:
                models = []
                for i in self.__get_all_table(cur):
                    models.append(Model(cur=cur, Name=i))
        return tuple(models)

    def update_models(self):
        self.Models = self.__get_tables()

    def get_model(self, model_name: str):
        for i in self.Models:
            if i.DefaultName == model_name or i.Name == model_name:
                return i
        raise ModelNotFound(f'Table with name {model_name} not found')
