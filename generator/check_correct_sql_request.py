from generator.SQL import SQLGen
from generator.SQLi import SQLiGen
from generator.errors import ErrorSQLRequest

from typing import Union
from mysql.connector import connect
from mysql.connector.cursor_cext import CMySQLCursor


class CheckCorrectRequest:
    def __init__(self, *args, **kwargs):
        self.Host = kwargs.get('Host')
        self.Port = kwargs.get('Port')
        self.User = kwargs.get('User')
        self.Password = kwargs.get('Password')
        self.Db = kwargs.get('Db')
        self.error = {'SELECT': [], 'INSERT': [], 'UPDATE': [], 'DELETE': [], 'UNION': []}

    def check_request(self, sql_gen_class: Union[SQLGen, SQLiGen], count: int = 1, type_check=None):
        """
        :param sql_gen_class: класс который генерит запросы
        :param count: количество генераций
        :param type_check: тип запроса 0-select, 1-insert, 2-update, 3-delete, 4-union, -1-все типы
        :return: bool
        """
        self.error = {'SELECT': [], 'INSERT': [], 'UPDATE': [], 'DELETE': [], 'UNION': []}
        with connect(host=self.Host, port=self.Port,
                     user=self.User, password=self.Password,
                     db=self.Db, ) as connection:
            with connection.cursor(buffered=True) as cur:
                for i in range(count):
                    if type_check in [0, -1]:
                        self.__check_select_request(sql_gen_class, count, cur)
                    elif type_check in [1, -1]:
                        self.__check_insert_request(sql_gen_class, count, cur)
                    elif type_check in [2, -1]:
                        self.__check_update_request(sql_gen_class, count, cur)
                    elif type_check in [3, -1]:
                        self.__check_delete_request(sql_gen_class, count, cur)
                    elif type_check in [4, -1]:
                        self.__check_union_request(sql_gen_class, count, cur)
        return self.error

    def __check_select_request(self, sql_gen_class: Union[SQLGen, SQLiGen], count: int, cur: CMySQLCursor):
        try:
            req = sql_gen_class.get_select_request()
            cur.execute(req)
        except Exception as e:
            self.error['SELECT'].append(req)

    def __check_insert_request(self, sql_gen_class: Union[SQLGen, SQLiGen], count: int, cur: CMySQLCursor):
        try:
            req = sql_gen_class.get_insert_request()
            cur.execute(req)
        except Exception as e:
            self.error['INSERT'].append(req)

    def __check_update_request(self, sql_gen_class: Union[SQLGen, SQLiGen], count: int, cur: CMySQLCursor):
        try:
            req = sql_gen_class.get_update_request()
            cur.execute(req)
        except Exception as e:
            self.error['UPDATE'].append(req)

    def __check_delete_request(self, sql_gen_class: Union[SQLGen, SQLiGen], count: int, cur: CMySQLCursor):
        try:
            req = sql_gen_class.get_delete_request()
            cur.execute(req)
        except Exception as e:
            self.error['DELETE'].append(req)

    def __check_union_request(self, sql_gen_class: Union[SQLGen, SQLiGen], count: int, cur: CMySQLCursor):
        try:
            req = sql_gen_class.get_union_request()
            cur.execute(req)
        except Exception as e:
            self.error['UNION'].append(req)
