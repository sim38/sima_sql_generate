
import random

from typing import Union
from generator.model_data import Model, Field
from generator.errors import FieldKeyIsNone
from generator.SQLABC import SQL, Template, Requests


class SQLGen(SQL):
    def __init__(self, model: Model, *args, **kwargs):
        self.model = model
        self.requests = Requests()

    def _get_select(self, fields: Union[list, tuple, None] = None) -> str:
        if fields is None:
            fields = self.model.random_count_field
        if self.requests.last_request.GroupByField:
            fields = ', '.join([random.choice(field.Base_data_type.AggrFunc).format(field.Name) for field in fields])
            fields = self.requests.last_request.GroupByField.Name + ', ' + fields
        else:
            fields = ', '.join([field.Name for field in fields])
        return Template.SELECT_REQUEST.format(fields, self.model.Name)

    def _get_insert(self, fields: Union[list, tuple, None] = None, count: int = 1) -> str:
        if fields is None:
            fields = self.model.Fields
        field_names = ', '.join([field.DefaultName for field in fields])
        field_values = ''
        for i in range(count):
            field_values += '(' + ', '.join([field.values for field in fields]) + ')'
            if i != count - 1:
                field_values += ', '
        return Template.INSERT_REQUEST.format(self.model.Name, field_names, field_values)

    def _get_delete(self):
        return Template.DELETE_REQUEST.format(
            self.model.Name
        ) + self._get_where()

    def _get_union(self):
        fields = self.model.random_count_field
        return Template.UNION_REQUEST.format(
            self._get_select(fields),
            self._get_where(),
            self._get_select(fields),
            self._get_where()
        )

    def _get_where(self, fields: list[Field] = None) -> str:
        if fields is None:
            fields = self.model.random_count_field
        res = Template.WHERE_BASE_REQUEST
        for field in fields:
            if res != Template.WHERE_BASE_REQUEST:
                res += ' and '
            res += Template.WHERE_REQUEST.format(
                        field.Name,
                        random.choice(field.Base_data_type.WhereOperators),
                        field.values
                    )
        return res

    def _get_group_by(self, field: Field = None) -> tuple:
        if field is None:
            field = self.model.random_field
        return Template.GROUP_BY_REQUEST.format(field.Name), field

    def _get_order_by(self, field: Field = None) -> str:
        if field is None:
            field = self.model.random_field
        if self.requests.last_request.GroupByField:
            field = self.requests.last_request.GroupByField
        return Template.ORDER_BY_REQUEST.format(
            field.Name,
            random.choice(Template.ORDER_BY_SORT)
        )

    def _get_having(self, field: Field = None) -> str:
        if field is None:
            field = self.model.random_field
        return Template.HAVING_REQUEST.format(
            random.choice(Template.HAVING_AGG_FUNC).format(field.Name),
            random.choice(Template.HAVING_OPERATORS),
            field.values
        )

    def _get_update(self) -> str:
        return Template.UPDATE_REQUEST.format(
            self.model.Name,
            ', '.join([f'{field.Name} = {field.values}' for field in self.model.random_count_field])
        )

    def _get_join(self, fields: Field = None) -> str:
        if fields is None:
            fields = self.model.random_foreign_key_field
            if fields is None:
                return ''
        if fields.Key is None:
            raise FieldKeyIsNone(f'Поле {fields.Name} таблицы {self.model.Name} не ForeignKey')
        return Template.JOIN_REQUEST.format(
            random.choice(Template.JOIN_TYPE),
            fields.key_database,
            fields.Name,
            fields.Key,
        )

    def get_select_request(self) -> str:
        new_request = self.requests.new_request()
        if new_request.SelectTemplateGroupBy:
            new_request.GroupBy, self.requests.last_request.GroupByField = self._get_group_by()
        if new_request.SelectTemplateJoin:
            new_request.Join = self._get_join()
        if new_request.SelectTemplateWhere:
            new_request.Where = self._get_where()
        if new_request.SelectTemplateHaving:
            new_request.Having = self._get_having()
        if new_request.SelectTemplateOrderBy:
            new_request.OrderBy = self._get_order_by()
        new_request.Select = self._get_select()
        return new_request.request

    def get_insert_request(self) -> str:
        return self._get_insert()

    def get_update_request(self) -> str:
        return self._get_update()

    def get_delete_request(self) -> str:
        return self._get_delete()

    def get_union_request(self) -> str:
        return self._get_union()
