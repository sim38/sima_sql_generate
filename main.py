import argparse
import time

from typing import Union

from generator.model_data import Database
from generator.SQL import SQLGen
from generator.SQLi import SQLiGen
from generator.generate_table import GenerateTable
from generator.check_correct_sql_request import CheckCorrectRequest

host = 'localhost'
port = 3306
user = 'root'
password = 'toor'
db = 'database'


base = Database(Host=host, Port=port, User=user, Password=password, Db=db)


parser = argparse.ArgumentParser(description='Генерируем ипроверяем запросы')
parser.add_argument('-gen', '--generate_table', default=0, type=int, help='Запускает генератор значений таблицы')
parser.add_argument('-test', '--test_request', default=0, type=int, help='Генерирует и проверяет правильность запросов')
parser.add_argument('-print', '--print_request', default=0, type=int, help='Генерирует и печатает запросы')
parser.add_argument('-type', '--type_request', default=-1, type=int, help='0-select, 1-insert, 2-update')
parser.add_argument('-name', '--table_name', default=None, type=str, help='название таблицы (для генерации)')
parser.add_argument('-w', '--write_csv', default=1, type=int, help='Сохранить данные в csv (для генерации)')
parser.add_argument('-i', '--injection', help='Использовать генератор инъекций', action="store_true")


args = parser.parse_args()


def generate_table(count: int, name: str = None):
    if name is not None:
        GenerateTable(model=base.get_model(name), host=host, port=port, user=user, password=password, db=db).run(count=count)
        return
    for model in base.Models:
        base.update_models()
        GenerateTable(model=model, host=host, port=port, user=user, password=password, db=db).run(count=count)


def test_request(count: int, _type: int, sql_gen_class: Union[SQLGen, SQLiGen]):
    # чтобы тесты были без ошибок foreign key надо удалять каскадом
    # и SET FOREIGN_KEY_CHECKS = 0;
    for model in base.Models:
        m = sql_gen_class(model)
        error = CheckCorrectRequest(
            Host=host, Port=port, User=user, Password=password, Db=db
        ).check_request(m, count, _type)
        print(f'{model.DefaultName}:')
        print(f'(good/bad request) SELECT: {count - len(error["SELECT"])}/{len(error["SELECT"])}, '
              f'INSERT: {count - len(error["INSERT"])}/{len(error["INSERT"])}, '
              f'UPDATE: {count - len(error["UPDATE"])}/{len(error["UPDATE"])}, '
              f'DELETE: {count - len(error["DELETE"])}/{len(error["DELETE"])}, '
              f'UNION: {count - len(error["UNION"])}/{len(error["UNION"])}')
        for i in error:
            print(f'\t{i}:')
            for j in error[i]:
                print(f'\t\t{j}')


def print_request(count: int, _type: int, sql_gen_class: Union[SQLGen, SQLiGen]):
    result = {}
    for model in base.Models:
        m = sql_gen_class(model)
        result[model.DefaultName] = {'SELECT': [], 'INSERT': [], 'UPDATE': [], 'DELETE': [], 'UNION': []}
        for i in range(count):
            res = check_type(
                _type, m.get_select_request, m.get_insert_request, m.get_update_request,
                m.get_delete_request, m.get_union_request
            )
            for j in res:
                result[model.DefaultName][j].append(res[j])
    str_res = ''
    for model in result:
        str_res += f'{model}: \n'
        for type_ in result[model]:
            str_res += f'\t{type_}:\n'
            for request in result[model][type_]:
                str_res += f'\t\t{request};\n'
    print(str_res)


def write_request(count: int, file_name='data.csv'):
    str_res = 'IntValues|StrValues\n'
    n = -1
    for sql_gen_class in [SQLGen, SQLiGen]:
        result = {}
        n += 1
        for model in base.Models:
            m = sql_gen_class(model)
            result[model.DefaultName] = {'SELECT': []}
            for i in range(count):
                res = check_type(
                    0, m.get_select_request, m.get_insert_request, m.get_update_request,
                    m.get_delete_request, m.get_union_request
                )
                for j in res:
                    result[model.DefaultName][j].append(res[j])
        for model in result:
            for type_ in result[model]:
                for request in result[model][type_]:
                    str_res += f'{n}|{request}\n'
    with open('/home/vova/PycharmProjects/sima_sql_generate/' + file_name, 'w') as file:
        file.write(str_res)


def check_type(_type, select, insert, update, delete, union) -> dict:
    """
    :param _type: тип запроса 0-select, 1-insert, 2-update, 3-delete, 4-union, -1 - все
    :param select: функция для select запроса
    :param insert: функция для insert запроса
    :param update: функция для update запроса
    :return:
    """
    res = {}
    if _type == 0 or _type == -1:
        res['SELECT'] = select()
    if _type == 1 or _type == -1:
        res['INSERT'] = insert()
    if _type == 2 or _type == -1:
        res['UPDATE'] = update()
    if _type == 3 or _type == -1:
        res['DELETE'] = delete()
    if _type == 4 or _type == -1:
        res['UNION'] = union()
    return res


t_start = time.time()

sql_gen_class = SQLiGen if args.injection else SQLGen

if args.write_csv:
    write_request(args.write_csv)
if args.generate_table:
    generate_table(args.generate_table, args.table_name)
if args.test_request:
    test_request(args.test_request, args.type_request, sql_gen_class)
if args.print_request:
    print_request(args.print_request, args.type_request, sql_gen_class)


print(f'Затраченное время: {time.time() - t_start} сек')


"""
CREATE TABLE `int` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `a` tinyint DEFAULT NULL,
  `b` int DEFAULT NULL,
  `c` bigint DEFAULT NULL,
  `d` smallint DEFAULT NULL,
  PRIMARY KEY (`id`)
) 
"""

"""
CREATE TABLE `test` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `a` date DEFAULT NULL,
  `b` text,
  `d` tinyint(1) DEFAULT NULL,
  KEY `id` (`id`),
  CONSTRAINT `test_ibfk_1` FOREIGN KEY (`id`) REFERENCES `int` (`id`) ON DELETE CASCADE
)
"""
